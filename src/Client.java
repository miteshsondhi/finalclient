import java.awt.BorderLayout;
import java.awt.Event;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.jws.Oneway;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

public class Client extends JFrame {
	private JTextField userText;
	private JScrollPane chatWindow;
	private JTextArea chat;
	private ObjectOutputStream output;
	private ObjectInputStream input;
	private ServerSocket server;
	private Socket connection;
	private JTextField port;
	private JButton connectButton;
	private JPanel panel;
	private int portValue;
	private JTextField ip;
	private Thread thread;
	private String message = "";

	// constructor
	public Client() {
		panel = new JPanel();
		panel.setLayout(null);

		// start server button
		connectButton = new JButton("Connect Button");
		connectButton.setSize(125, 25);
		connectButton.setLocation(25, 5);
		panel.add(connectButton);
		connectButton.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				thread = new Thread(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						try {
							startConnection();
							setStreams();
							recieveData();
						} catch (ClassNotFoundException | IOException e) {

							e.printStackTrace();
						}
					}
				});
				thread.start();
			}

			// do nothing
			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		});

		// port
		port = new JTextField("8888");
		port.setSize(50, 25);
		port.setLocation(280, 5);
		panel.add(port);

		// ip
		ip = new JTextField("localhost");
		ip.setSize(100, 25);
		ip.setLocation(170, 5);
		panel.add(ip);

		// chat window
		chat = new JTextArea();
		chat.setSize(300, 150);
		chat.setLocation(25, 75);
		chatWindow = new JScrollPane(chat);
		chatWindow.setSize(300, 150);
		chatWindow.setLocation(25, 75);
		panel.add(chatWindow);
		// user text
		userText = new JTextField();
		userText.setSize(300, 25);
		userText.setLocation(25, 235);
		panel.add(userText);
		userText.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					sendData();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}

			private void sendData() throws IOException {
				if (!connection.isClosed()) {
					output.writeObject(userText.getText());
					output.flush();
					if (userText.getText().equals("EXIT")) {
						message = "EXIT";
					}
					showMessage("Client :" + userText.getText());
					userText.setText("");
				} else {
					userText.setText("");
					showMessage("Please check your connection");
				}
			}
		});

		// panel
		this.add(panel);
		this.setSize(350, 300);
		this.setVisible(true);

	}

	public void showMessage(final String string) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				chat.append("\n" + string);

			}
		});
	}

	// function for open a port
	public void startConnection() throws IOException, ClassNotFoundException {
		portValue = Integer.parseInt(port.getText());
		String ipAddress = ip.getText();

		connection = new Socket(ipAddress, portValue);
	}

	public void closeConnections() throws IOException {

		connection.close();
		input.close();
		output.close();
		message = "";
	}

	public void recieveData() throws ClassNotFoundException, IOException {

		while (!message.equals("EXIT")) {
			try {
				message = (String) input.readObject();
				showMessage("Server : " + message);

			} catch (IOException e) {
			}
		}
		closeConnections();

	}

	public void setStreams() throws IOException {
		output = new ObjectOutputStream(connection.getOutputStream());
		output.flush();
		input = new ObjectInputStream(connection.getInputStream());
		showMessage("Connection is established");
	}

}
